FROM ubuntu:20.04

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt-get update && \
    apt-get install -y  python3.11 build-essential libxml2-dev python3-dev python3-pip zlib1g-dev python3-requests && \
    pip install numpy==1.21.5 && \
    pip install anndata==0.8.0 && \
    pip3 install cellxgene

ENTRYPOINT ["cellxgene"]
